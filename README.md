# Apache docker image with FPM support

[![pipeline status](https://gitlab.com/kalmac/apache/badges/master/pipeline.svg)](https://gitlab.com/kalmac/apache/commits/master)

1. [Description](#description)
2. [Available tags](#available-tags)
3. [Configuration](#configuration)

# Description

This docker image is built on [httpd](https://hub.docker.com/_/httpd/).
It adds support for php-fpm, can protect content with a basic http authentification and can block bots from indexing content.
All this [configuration](#configuration) can be done using environment variables.

Changes are applied without rebuilding image.

# Available tags

> **Pull command** :
> docker pull [registry.gitlab.com/kalmac/apache:2.4](https://gitlab.com/kalmac/apache/container_registry)

* 2.4 (latest)

## Configuration

Configuration is done by passing env variables at run command :
```
docker run -e DISALLOW_ROBOTS=false -d registry.gitlab.com/kalmac/apache:2.4
```

| variable        | Description                                                        | Default value |
|-----------------|--------------------------------------------------------------------|---------------|
| PHP_HOST        | IP or hostname where PHP fpm is running.                           | php-fpm       |
| PHP_PORT        | PHP fpm port.                                                      | 9000          |
| PHP_PATH        | Common document root for both apache & php fpm (without trailing /)| /var/www/html |
| DISALLOW_ROBOTS | If true, a robots.txt is used to forbid access to search engines.  | true          |
| HT_PASSWORD     | If defined, an authorization is required to access server.         | (empty)       |
| HT_LOGIN        | Default login used only if a password is defined.                  | anonymous     |
| PROXY_TIMEOUT   | Timeout delay for PHP proxy requests.                              | 60            |
